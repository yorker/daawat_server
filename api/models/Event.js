/**
 * Event.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
  	
  	event_name: { 
  		type: 'string',
  		required: true
  	},
  	
  	event_date: { 
  		type: 'string',
  		required: true
  	},

  	event_time: 'string',
  	
  	event_type: {
  		type: 'string',
  		enum: ['Birthday', 'Thanksgiving', 'Christmas', 'New Year\'s Eve', 'Get Together', 'Pool Party', 'Graduation', 'Other']
    },

    created_by: {
      model: 'user'
    }, 

    guests: {
      type: 'array'
    }
  }
};

