
function createEvent (data, onCreateEventSuccess, onCreateEventError) {
	var url = "/event/create";


	/**
	 TODO: This was failing with "Allow-Control-Allow-Origin" error. 
	 So I installed "Allow-Control-Allow-Origin" Chrome plugin to 
	 make it work for the demo. We need to figure a proper solution.
	 */
	$.ajax({
	    url : url,
	    type: "POST",
	    data : data,
	    success: function(data, textStatus, jqXHR) {
	        console.log("Successfully saved the data");
	        parent.onCreateEventSuccess(data);
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	    	console.log("Failed to save the data");
	    	parent.onCreateEventError(errorThrown);
	    }
	});
}


var g_logindata;

function loginToASDA(onASDALoginSuccess, onASDALoginError) {
	var url = "https://groceries.asda.com/api/user/login?email=johngummadi@gmail.com&password=win32api&rememberme=true&requestorigin=codeception";

	$.ajax({
	    url : url,
	    type: "POST",
	    success: function(data, textStatus, jqXHR) {
	        console.log("Successfully logged into ASDA");
	        saveLoginCookie(data);
	        parent.onASDALoginSuccess();
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	    	console.log("Failed to login to ASDA");
	    	clearLoginCookie();
	    	parent.onASDALoginError(errorThrown);
	    }
	});
	//https://groceries.asda.com/api/user/login?email=sample@gmail.com&password=password&rememberme=true&requestorigin=gi
}

function addToCart(key) {
	var item = g_promoList[key];
	if (!item)
		return;

	var url = "https://groceries.ASDA.com/api/cart/additem&basketid=" + g_logindata.basketid + "& itemids=" + item.id + "&quantities=1&apikey=7ucbnywa6thu5yrmdd39rz94&requestorigin=codeception";

	$.ajax({
	    url : url,
	    type: "POST",
	    success: function(data, textStatus, jqXHR) {
	        console.log(item.name + " successfully added to cart");
	        saveLoginCookie(data);
	        onASDALoginSuccess(); // This will load the cart again
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	    	console.log(item.name + " failed to add to cart");
	    	clearLoginCookie();
	    }
	});
}

function getCookie(cname) {
	var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}

function setCookie(cname, cvalue) {
    document.cookie = cname + "=" + cvalue + ";";
}

function getLoginCookie() {
	var logindataStr = getCookie("logindata");
	return JSON.parse(logindataStr);
}

function saveLoginCookie(data) {
	setCookie("logindata", JSON.stringify(data));
}

function clearLoginCookie() {
	setCookie("logindata", null);
}

var g_cart;
function loadCart(onCartLoadSuccess, onCartLoadError) {
	// TODO
	//
	var url = "https://groceries.asda.com/api/cart/view";
	$.ajax({
	    url : url,
	    type: "GET",
	    success: function(data, textStatus, jqXHR) {
	        console.log("Successfully retrieved shopping list items");
	        g_cart = data;
	        parent.onCartLoadSuccess(data);
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	    	console.log("Failed to retrieve shopping list items");
	    	g_cart = null;
	    	parent.onCartLoadError(errorThrown);
	    }
	});
}

function getCart() {
	return g_cart;
}

function getProduct(keyword, onGetProductSuccess, onGetProductError) {
	//https://api-groceries.asda.com/api/items/search?apikey=7ucbnywa6thu5yrmdd39rz94&requestorigin=android&keyword=chicken&pagenum=1
	var url = "https://api-groceries.asda.com/api/items/search?apikey=7ucbnywa6thu5yrmdd39rz94&requestorigin=android&keyword=" + keyword + "&pagenum=1";
	$.ajax({
	    url : url,
	    type: "GET",
	    success: function(data, textStatus, jqXHR) {
	        console.log("Successfully retrieved product the data for " + keyword);
	        parent.onGetProductSuccess(data);
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	    	console.log("Failed to retrieve product the data for " + keyword);
	    	parent.onGetProductError(errorThrown);
	    }
	});
}

function createElement(type, id, className, innerHTML, parent) {
	var elem = document.createElement(type);
	
	if (parent)
		parent.appendChild(elem);

	if (id)
		elem.setAttribute("id", id);

	if (className)
		elem.setAttribute("class", className);
		// elem.class = className;

	if (innerHTML)
		elem.innerHTML = innerHTML;

	return elem;
}

var g_basketListCache = {};
function addToBasketListCache(key, promoData) {
	g_basketListCache[key] = promoData;
}

function removeFromBasketListCache(key) {
	delete g_basketListCache[key];
}

function updateCartAmount() {
	var totalPrice = 0;

	for (var key in g_basketListCache) {
		var item = g_basketListCache[key];
		//item.price.replace(/[^\d.]/g, '')
		totalPrice += Number(item.price.replace('£', ''));
		//totalPrice += item.price.replace('£', '');
	}
	var totalPriceStr = parseFloat(Math.round(totalPrice * 100) / 100).toFixed(2);
	$('#basket_amount').html("£" + totalPriceStr);
}

var g_promoList = {};
function addPromotion(key, promoData) {
	g_promoList[key] = promoData;
}

function removePromotion(key) {
	delete g_promoList[key];
}

function getPromoList() {
	return g_promoList;
}

function onAddToCart(key) {
	addToCart(key);
	//alert(g_promoList[key].name);
}

function showPromotion(promoData, keyword, promotionsDivId) {
	if (promoData == null)
		return;

	addPromotion(keyword, promoData);

	var promotionsDiv = document.getElementById(promotionsDivId);

	var promoDiv = createElement("tr", keyword, "promotion", null, promotionsDiv);
	
	var promoAnchor = createElement("a", null, null, null, promoDiv);
	//promoAnchor.href = promoData.productURL;
	//promoAnchor.target = "_blank";

	var img = createElement("img", null, "promotion_col", null, promoAnchor);
	img.src = promoData.imageURL;

	var rightSpan = createElement("span", null, "promotion_col", null, promoAnchor);

	var titleText = createElement("div", null, null, promoData.itemName, rightSpan);

	var priceText = createElement("div", null, null, promoData.pricePerUOM, rightSpan);

	var addToCart = createElement("button", null, "btn btn-success cart_button", "Add to Cart", rightSpan);

	addToCart.onclick = (function(key) {
	    return function() {
	    	if (addToCart.innerHTML == "Remove") {
	    		addToCart.setAttribute("class", "btn btn-success cart_button");
	    		addToCart.innerHTML = "Add to Cart";
	    		removeFromBasketListCache(key);
	    		updateCartAmount();
	    	}
	    	else {
	    		onAddToCart(key);
	    		addToCart.setAttribute("class", "btn btn-danger cart_button");
	    		addToCart.innerHTML = "Remove";
	    		addToBasketListCache(key, g_promoList[key]);
	    		updateCartAmount();
	    	}
	    };
	})(keyword);


	// Scroll to end
	promotionsDiv.scrollTop = promotionsDiv.scrollHeight;
}
